Braintree Front-End Code Challenge repository for Andi Wilson.

# Technologies
* Front-end written in vanilla Javascript without libraries or frameworks. 
* Simple back-end built with Python Flask (form validation through WTForms).
* CSS written pre-compiled in SCSS.
* Package dependencies managed by PIP.

# Local web server requirements

The following commands are required to run a local web server.

### pip
* Install easy_install: `curl https://bootstrap.pypa.io/ez_setup.py -o - | sudo python`
* Install pip: `sudo easy_install pip`

### virtualenv
* Install `virtualenv`: `sudo pip install virtualenv`

# Application setup instructions

**Important: Port 5000 must be available.**

* Clone this repository into your development dirctory: `git clone git@bitbucket.org:pdxbmw/braintree.git`
* Change into the cloned (root) directory: `cd braintree`
* Execute the run script: `./run.sh`
* Web server should be running at http://127.0.0.1:5000. 

*If there are issues, you can manually complete the following steps.*

* Create Python virtual environment: `virtualenv venv`
* Activate the virtual environment: `source venv/bin/activate`
* Install Python Flask requirements: `pip install -r requirements.txt`
* Start a web server (from project root): `python app/app.py`

# Jasmine test instructions

**Important: The following requires a virtual environment created in the above steps.**

* Change into the root directory: `cd braintree`
* Execute the test script: `./test.sh`
* Jasmine Spec Runner should be running at http://127.0.0.1:8888.

*If there are issues, you can manually complete the following steps.*

* Activate the virtual environment: `source venv/bin/activate`
* Change into the test directory: `cd braintree/app/test`
* Run command: `jasmine`

# TODO
* Create further Jasmine tests.
* Create Python tests.