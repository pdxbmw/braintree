"""Braintree code challenge Python Flask application."""

import re
from flask import Flask, jsonify, make_response, render_template, request
from flask_assets import Environment, Bundle
from wtforms import Form, PasswordField, StringField, ValidationError, validators

app = Flask(__name__)

# Bundle assets, relative to `static` folder.
js = Bundle(
    'javascripts/src/helpers.js',
    'javascripts/src/store.js',
    'javascripts/src/template.js',
    'javascripts/src/model.js',
    'javascripts/src/view.js',
    'javascripts/src/controller.js',
    'javascripts/src/app.js',
    depends='javascripts/src/*.js',
    filters='jsmin',
    output='javascripts/bundle.js')
scss = Bundle(
    'styles/scss/app.scss',
    depends='styles/scss/**/*.scss',
    filters='pyscss',
    output='styles/bundle.css')
assets = Environment(app)
assets.register('js_all', js)
assets.register('scss_all', scss)


####################
# Custom validators
####################

def validate_address(form, field):
    """Check if input contains a "PO Box" address."""
    if not field.data:
        return
    pattern = r'\bP(ost|ostal)?([ \.]*(O|0)(ffice)?)?([ \.]*Box)?\b'
    match = re.search(pattern, field.data, re.IGNORECASE)
    if not match:
        return
    raise ValidationError("Cannot be a P.O. Box address.")


def validate_ssn(form, field):
    """Check for a valid SSN (not all the same digit)."""
    if not field.data:
        return
    pattern = r'^([0-9])\1*$'
    match = re.search(pattern, field.data)
    if not match:
        return
    raise ValidationError("Not a valid Social Security Number.")


####################
# Forms
####################

class SignupForm(Form):
    """Signup form. Would obviously be in its own file."""

    first_name = StringField(
        label='First Name',
        validators=[
            validators.InputRequired()])
    last_name = StringField(
        label='Last Name',
        validators=[
            validators.InputRequired()])
    address = StringField(
        label='Address',
        validators=[
            validators.InputRequired(),
            validate_address])
    ssn = PasswordField(
        label='Social Security Number',
        validators=[
            validators.InputRequired(),
            validators.Length(min=9, max=9),
            validators.NoneOf(['123456789', '987654321']),
            validate_ssn])
    email = StringField(
        label='Email',
        validators=[
            validators.InputRequired(),
            validators.EqualTo('email_confirmation')])
    email_confirmation = StringField(
        label='Confirm Email',
        validators=[
            validators.InputRequired(),
            validators.EqualTo('email')])


####################
# Routes
####################

@app.route('/')
def index():
    """Serve signup as the homepage."""
    form = SignupForm()
    return render_template('form.html', form=form)


@app.route('/application', methods=['POST'])
def signup():
    """Signup endpoint with JSON response."""
    form = SignupForm(request.form)
    status_code = 201 if form.validate() else 400
    return make_response(jsonify(**form.errors), status_code)


if __name__ == '__main__':
    app.run()
