;(function (window) {
  'use strict';
  
  /**
   * Controller between a model and a view.
   *
   * @contructor
   * @param {object} model The model instance.
   * @param {object} view The view instance.
   */
  function Controller(model, view) {
    this.model = model;
    this.view = view;
        
    this.view.bind('formSubmit', this.submitForm.bind(this));
    this.view.bind('inputBlur', this.validateInput.bind(this));
    this.view.bind('ssnKeypress', this.validateSSN.bind(this));
  }

  /**
   * Handle SSN input `keypress`. Displays an error on keypress, if invalid.
   *
   * @param {HTMLElement} inputElement HTML input element.
   * @param {string} key The key character entered on keypress.
   */
  Controller.prototype.validateSSN = function (inputElement, key) {
    var errors, inputData = {};

    if (!inputElement || !inputElement.name || !key) {
      return;
    }

    inputData[inputElement.name] = key;
    errors = this.model.ensureDigits(inputData);

    this._handleValidation(errors);
  };

  /**
   * Handle input `blur` event.
   *
   * @param {HTMLElement} inputElement HTML input element.
   */
  Controller.prototype.validateInput = function (inputElement, formElement) {
    var errors, formData, inputData = {};

    if (!inputElement || !inputElement.name) {
      return;
    }

    inputData[inputElement.name] = inputElement.value;
    formData = serialize(formElement);
    errors = this.model.validate(formData, inputData);

    this._handleValidation(errors);
  };

  /**
   * Handle form `submit`.
   *
   * @param {HTMLElement} formElement HTML form element.
   */
  Controller.prototype.submitForm = function (formElement) {
    var self = this;
    var formData;

    if (!formElement) {
      return;
    }

    self.view.render('disableSubmit');

    formData = serialize(formElement);

    self.model.save(formData, function (response) {      
      
      if (!response.validated) {
        self._handleValidation(response.errors);
        return;
      }

      if (!response.success) {
        self.view.render('enableSubmit');
        self.view.render('saveIncomplete');        
        return;
      }
      self.view.render('saveComplete');
    });
  };


  /**
   * Handle form validation from the model.
   *
   * @param {array} errors An array of error objects from the model.
   *                                
   */
  Controller.prototype._handleValidation = function (errors) {
    var errorObj;
    var i = errors.length;

    while (i--) {
      errorObj = errors[i];

      // Display error(s) and continue.
      if (errorObj.errors.length) {
        this.view.render('showError', errorObj);
        continue;
      }
      
      this.view.render('clearError', errorObj);
    }

    this.view.render('enableSubmit');
  };
  
  // Export to window.
	window.app = window.app || {};
	window.app.Controller = Controller;
})(window);