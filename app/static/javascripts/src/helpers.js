;(function (window) {
	'use strict';


  //
  // Local functions.
  // --------------------------------------------------


  var classNamePattern = function (className) {
    return /(\\s|^)' + className + '(\\s|$)/;
  };


  //
  // General.
  // --------------------------------------------------


  // Allow for looping on nodes by chaining.
  // Example: qsa('.foo').forEach(function () {})
  NodeList.prototype.forEach = Array.prototype.forEach;


  //
  // DOM element searching.
  // --------------------------------------------------


  // Get element by CSS selector.
  window.qs = function (selector, scope) {
    return (scope || document).querySelector(selector);
  };

  // Get element(s) by CSS selector.
  window.qsa = function (selector, scope) {
    return (scope || document).querySelectorAll(selector);
  };

  // Get element by CSS class name.
  window.qsc = function (selector, scope) {
    return window.qs('.' + selector, scope);
  };

  // Get element by type and name attribute.
  window.qsn = function (type, name, scope) {
    return window.qs(type + '[name="' + name + '"]', scope);
  };

  // Get element(s) by CSS class name.
  window.qsac = function (selector, scope) {
    return window.qsa('.' + selector, scope);
  };

  // Find the element's parent with the given tag name.
  // Example: $parent(qs('a'), 'div');
  window.$parent = function (element, tagName) {
    if (!element.parentNode) {
      return;
    }
    // Match.
    if (element.parentNode.tagName.toLowerCase() === tagName.toLowerCase()) {
      return element.parentNode;
    }
    // Continue traversing up the DOM tree.
    return window.$parent(element.parentNode, tagName);
  };


  //
  // Event listener shortcuts.
  // --------------------------------------------------


  // addEventListener wrapper.
  window.$on = function (target, type, callback, useCapture) {
    
    if (!target) {
      return;
    }

    target.addEventListener(type, callback, !!useCapture);
  };

  // Attach a handler to event for all elements that match the selector,
  // now or in the future, based on a root element.
  window.$delegate = function (target, selector, type, handler) {
    var useCapture;

    function dispatchEvent(event) {
      var targetElement = event.target;
      var potentialElements = window.qsa(selector, target);
      var hasMatch = Array.prototype.indexOf.call(potentialElements, targetElement) >= 0;

      if (hasMatch) {
        handler.call(targetElement, event);
      }
    }

    // https://developer.mozilla.org/en-US/docs/Web/Events/blur
    useCapture = type === 'blur' || type === 'focus';

    window.$on(target, type, dispatchEvent, useCapture);
  };


  //
  // DOM element class utlities.
  // --------------------------------------------------


  // Check if element has a given class name.
  window.hasClass = function (element, className) {
    if (element.classList) {
      return element.classList.contains(className);
    }
    return element.className.match(classNamePattern(className));
  };

  // Add a given class name to an element.
  window.addClass = function (element, className) {
    if (hasClass(element, className)) {
      return;
    }
    if (element.classList) {
      element.classList.add(className);
      return;
    }
    element.className = element.className += ' ' + className;
  };

  // Remove a given class name from an element.
  window.removeClass = function (element, className) {
    if (!hasClass(element, className)) {
      return;
    }
    if (element.classList) {
      element.classList.remove(className);
      return;
    }
    element.className = element.className.replace(classNamePattern(className), ' ');
  };


  //
  // Form element utilities.
  // --------------------------------------------------


  // Convert a form's input elements to object of key-value pairs.
  window.serialize = function (formElement) {
    var inputs, data, i;

    if (!formElement) {
      return;
    }

    inputs = [].slice.call(qsa('input', formElement));    
    i = inputs.length;
    data = {};

    // NOTE: This will reverse the order.
    while (i--) {
      data[inputs[i].name] = inputs[i].value;
    }

    return data;
  };

  // Check for empty string.
  window.isEmptyString = function (value) {
    return value === '';
  };

  // Check for equal strings.
  window.isEqual = function (str1, str2) {
    return str1 === str2;
  };

  // Check if input contains a "PO Box" address.
  // http://stackoverflow.com/a/34140301
  window.isPOBox = function (value) {
    var pattern = /\bP(ost|ostal)?([ \.]*(O|0)(ffice)?)?([ \.]*Box)?\b/i;
    return value.match(pattern) !== null;
  };

  // Check for a valid SSN (not all the same digit).
  window.isValidSSN = function (value) {
    var blacklist = ['123456789', '987654321'];
    var pattern = /^([0-9])\1*$/;
    return value.length === 9 && blacklist.indexOf(value) === -1 && value.match(pattern) === null;
  };

  window.isDigit = function (value) {
    return value.match(/^[0-9]+$/) !== null;
  };
})(window);