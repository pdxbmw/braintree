;(function(window) {
  'use strict';
  
  /**
   * Creates a Model instance.
   *
   * @constructor
   */
  function Model(store) {
    this.store = store;
  }

  /**
   * Validates the passed-in form data and if there are no validation errors,
   * the form data is handed to the local store module to handle saving to the
   * backend. If there are errors, the array of validation errors is passed
   * back to the controller through the supplied callback function.
   *
   * @param {object} formData Complete serialized form data. Always passed in
   *                          to allow for email field comparison.
   * @param {function} callback The callback function to execute when completed.
   */
  Model.prototype.save = function (formData, callback) {
    var errors, isValidated, response;
    var isSaved = false;

    if (!formData || !callback) {
      return;
    }

    errors = this.validate(formData);  

    // True *only* if no error object has errors.
    isValidated = errors.filter(function (i) { return i.errors.length; }).length === 0;

    response = { errors: errors, validated: isValidated };

    // Return if there are validation errors.    
    if (!isValidated) {
      callback(response);
      return;
    }

    formData.submitted_at = new Date();

    this.store.save(formData)
      .then(
        function (success) { response.success = success; },
        function (error) { response.success = error; })
      .then(
        function () { callback(response); });
  };

  /**
   * Validates form or input data. Returns an array of objects containing the
   * name of the input field and the associated errors. 
   *
   * NOTE: Will return an empty array for input fields without errors. This is
   *       necessary for the controller to clear outstanding input errors, if
   *       they exist.
   *
   * @param {object} formData Complete serialized form data. Always passed in
   *                          to allow for email field comparison.
   * @param {object|undefined} inputData Serialized input field. This value
   *                                     takes precedence over the form data,
   *                                     if it's defined. This allows for
   *                                     single input field validation.
   *
   * @returns {array} An array of objects containing the name of the input
   *                  field and the associated errors. 
   */
  Model.prototype.validate = function (formData, inputData) {
    var name, value, errors, otherEmail;
    var data = inputData || formData;
    var output = [];

    // Loop through input fields and validate values.
    for (name in data) {
      value = data[name];
      errors = [];

      // All fields are required.
      if (isEmptyString(value)) {
        errors.push('isRequired');
      } 
      // Address validation.
      else if (name === 'address') {
        if (isPOBox(value)) {
          errors.push('invalidAddress');
        }
      }
      // Email confirmation matching.
      else if (name === 'email_confirmation') {
        otherEmail = formData.email;
        if (!isEmptyString(otherEmail) && !isEqual(value, formData.email)) {
          errors.push('emailMismatch');
        }
      } 
      // Email confirmation matching.
      else if (name === 'email') {
        otherEmail = formData.email_confirmation;
        if (!isEmptyString(otherEmail) && !isEqual(value, otherEmail)) {
          errors.push('emailMismatch');
        }
      }            
      // SSN validation.
      else if (name === 'ssn') {
        if (!isValidSSN(value)) {
          errors.push('invalidSSN');
        }
      }

      output.push({ name: name, errors: errors });
    }

    return output;
  };

  /** 
   * Special case to handle SSN keypress validation.
   *
   * @param {object} inputData Serialized input field.
   *
   * @returns {array} An single-item array of the field name and errors.
   *
   * @todo Reconsider as part of the `validate` method.
   */
  Model.prototype.ensureDigits = function (inputData) {
    var name, value, errors;
    var output = [];

    for (name in inputData) {
      value = inputData[name];
      errors = [];

      if (!isDigit(value)) {
        errors.push('onlyNumbers');
      }

      output.push({ name: name, errors: errors });
    }

    return output;
  };

  // Export to window.
  window.app = window.app || {};
  window.app.Model = Model;
})(window);