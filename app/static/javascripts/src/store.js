;(function(window) {
  'use strict';

  var POST_URL = '/application';

  /**
   * Handles interaction with the backend data store. 
   */
  function Store() {

  }

  /**
   * Simple XHR POST request.
   *
   * @param {string} url The URL to get.
   * @param {object} data Serialized data.
   *
   * @returns {boolean} True if saved, false if not.
   */
  Store.prototype.save = function (data) {
    return new Promise(function(resolve, reject) {
      var req = new XMLHttpRequest();
      var encodedData = '';
      var encodedPairs = [];
      var name;

      // Convert data object to array of URL encoded key-value pairs.
      for (name in data) {
        encodedPairs.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));
      }

      // Combine pairs into a single string and replace encoded spaces to plus
      // character to match behaviour of web browser form submit.
      encodedData = encodedPairs.join('&').replace(/%20/g, '+');    

      req.open('POST', POST_URL, true);
      req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      req.onload = function() { return resolve(req.status === 201); };
      req.onerror = function() { return reject(false); };
      req.send(encodedData);
    });
  };

  // Export to window.
  window.app = window.app || {};
  window.app.Store = Store;
})(window);