;(function(window) {
  'use strict';

  /**
   * Basic template-rendering engine.
   *
   * @constructor
   */
  function Template() {
    this.defaultTemplate = '<li>{{text}}</li>';
  }

  /**
   * Creates an HTML <span> element.
   *
   * @param {object} data The object containing keys to compile the template.
   * @returns {HTMLElement} The <span> element.
   *
   * @example
   * view.error({
   *     message: "Error message.",
   * });
   */
  Template.prototype.error = function (data) {
    var html = '';
    var template, i, l;

    for (i = 0, l = data.length; i < l; i++) {
      template = this.defaultTemplate;
      template = template.replace('{{text}}', data[i].text);
      html += template;
    }

    return html;
  };
  
  // Export to window
  window.app = window.app || {};
  window.app.Template = Template;
})(window);