;(function(window) {
  'use strict';

  var ZERO_DIGIT_KEYCODE      = 48;
  var NINE_DIGIT_KEYCODE      = 57;

  var FORM_CLASSNAME          = 'signup-form';
  var INPUT_CLASSNAME         = 'signup-form__input';
  var INPUT_ERROR_CLASSNAME   = 'signup-form__input--error';
  var ERROR_LIST_CLASSNAME    = 'signup-form__error-list';
  var SUBMIT_BUTTON_CLASSNAME = 'signup-form__submit-btn';
  var SAVE_CLASSNAME          = 'signup-form__save-response';
  var SAVE_ERROR_CLASSNAME    = 'signup-form__save-response--error';

  var errorText = {
    emailMismatch:  "Email addresses do not match.",
    invalidAddress: "Cannot be a P.O. Box address.",
    invalidSSN:     "Not a valid Social Security Number.",
    isRequired:     "This field is required.",
    onlyNumbers:    "Only numbers are allowed."
  };    

  /**
   * Handles the presentation logic.
   *
   * @constructor
   * @param {object} template The template instance.
   */
  function View(template) {
    this.template = template;

    this.$form = qsc(FORM_CLASSNAME);
    this.$inputs = qsac(INPUT_CLASSNAME, this.$form);
    this.$submit = qsc(SUBMIT_BUTTON_CLASSNAME, this.$form);
    this.$response = qsc(SAVE_CLASSNAME, this.$form);
  }

  /**
   * API called by the controller to render a given command with options.
   *
   * @param {string} command The command to execute.
   * @param {string} parameter The parameter(s) to pass to the render function.
   */
  View.prototype.render = function (command, parameter) {
    var self = this;
    var viewCommands;

    if (!command) {
      return;
    }

    viewCommands = {
      clearError:     this._clearError.bind(this),
      showError:      this._showError.bind(this),
      saveComplete:   this._saveDone.bind(this),
      saveIncomplete: this._saveError.bind(this),
      disableSubmit:  this._disableSubmit.bind(this),
      enableSubmit:   this._enableSubmit.bind(this)
    };

    viewCommands[command](parameter);
  };

  /**
   * Takes a signup event and registers the event handler.
   *
   * @param {string} event The View event name. 
   * @param {string} handler The event callback handler to execute.
   */
  View.prototype.bind = function (event, handler) {
    var self = this;

    if (!event || !handler) {
      return;
    }

    // Form is submitted.
    if (event === 'formSubmit') {
      $on(self.$form, 'submit', function(e) {  
        e.preventDefault();
        return handler(e.target);
      });
    // Keypress entry to SSN input field.
    } else if (event === 'ssnKeypress') {
      $delegate(self.$form, 'input[name="ssn"]', 'keypress', function(e) {
        
        // If value not a digit, prevent keypresss.
        if (e.which < ZERO_DIGIT_KEYCODE || e.which > NINE_DIGIT_KEYCODE) {
          e.preventDefault();
        }

        handler(e.target, e.key);
      });
    // Input field loses focus.
    } else if (event === 'inputBlur') {
      $delegate(self.$form, 'input', 'blur', function(e) {
        var otherElement, otherName;
        var currentTarget = e.target;

        e.preventDefault();
        handler(currentTarget, self.$form);       

        // Handle special emails-must-match case.
        //
        // NOTE: Both email input fields will be validated when one or the
        //       other loses focus. Both will display the `emailMismatch`
        //       error message as long as both fields contain values. 

        // Return if the target input is not an email field.
        if (currentTarget.name.indexOf('email') === -1) {
          return;
        } 

        // Determine the other email field that's not the current target.
        otherName = currentTarget.name === 'email' ? 'email_confirmation' : 'email';
        otherElement = qsn('input', otherName, self.$form);

        if (isEmptyString(currentTarget.value) || isEmptyString(otherElement.value)) {
          return;            
        }

        handler(otherElement, self.$form);
      });
    }
  };

  /**
   * Takes an error object from the model and creates and adds 
   * the error HTML element to the input form field.
   *
   * @param {object} errorObj An object that contains the name of the input
   *                          field and an array of errors.
   */
  View.prototype._showError = function (errorObj) {
    var inputElement, listElement, messages;


    if (!errorObj || !errorObj.name || !errorObj.errors) {
      return;
    }

    inputElement = qsn('input', errorObj.name, this.$form);

    if (!inputElement) {
      return;
    }

    listElement = qsc(ERROR_LIST_CLASSNAME, inputElement.parentNode);

    if (!listElement) {
      return;
    }

    // Format error messages for templating.
    messages = errorObj.errors.map(function (errorType) {
      return { text: errorText[errorType] };
    });

    listElement.innerHTML = this.template.error(messages);

    addClass(inputElement, INPUT_ERROR_CLASSNAME);
  };

  /**
   * Clears a single input field error.
   *
   * @param {object} errorObj An object that contains the name of the input
   *                          field and an array of errors.
   */
  View.prototype._clearError = function (errorObj) {
    var inputElement, listElement;

    if (!errorObj || !errorObj.name || !errorObj.errors) {
      return;
    }

    inputElement = qsn('input', errorObj.name, this.$form);

    if (!inputElement || !inputElement.parentNode) {
      return;
    }

    listElement = qsc(ERROR_LIST_CLASSNAME, inputElement.parentNode);

    if (!listElement) {
      return;
    }

    listElement.innerHTML = '';

    removeClass(inputElement, INPUT_ERROR_CLASSNAME);
  };

  /**
   * Shows the success response after save.
   */
  View.prototype._saveDone = function () {
    removeClass(this.$response, SAVE_ERROR_CLASSNAME);
    this.$response.innerHTML = "Sign-up was successful!";
  };

  /**
   * Shows the error response after save.
   *
   * NOTE: In the real world, the template would be re-rendered with the errors
   *       from the response. Since we're simply serving status codes, we'll
   *       just display a generic error.
   */
  View.prototype._saveError = function () {
    addClass(this.$response, SAVE_ERROR_CLASSNAME);
    this.$response.innerHTML = "There was an error while saving.";
  };

  /**
   * Enable the submit button.
   */
  View.prototype._enableSubmit = function () {
    this.$submit.disabled = false;
  };

  /**
   * Disable the submit button.
   */
  View.prototype._disableSubmit = function () {
    this.$submit.disabled = true;
  };

  // Export to window.
  window.app = window.app || {};
  window.app.View = View;
})(window);