describe('Controller', function () {
  'use strict';

  var subject, view, model;

  var formData = {
    first_name: 'Dummy',
    last_name: 'Name'
  };

  var inputElement = {
    name: 'inputName',
    value: 'inputValue'
  };

  var inputData = {
    inputName: 'inputValue'
  };

  var noErrors = [{
    name: 'first_name',
    errors: []
  },{
    name: 'last_name',
    errors: []
  }];

  var withErrors = [{
    name: 'first_name',
    errors: ['isRequired']
  },{
    name: 'ssn',
    errors: ['invalidSSN']
  }];

  var setupModel = function (errors) {

    model.validate.and.callFake(function (errors) {
      return errors;
    });
    
    model.save.and.callFake(function (response) {    
      return {
        validated: response.validated,
        success: response.success,
        errors: response.errors
      };
    });    
    
    model.ensureDigits.and.callFake(function (inputData) {
      return noErrors;
    });
  };

  var createViewStub = function () {
    var eventRegistry = {};
    return {
      render: jasmine.createSpy('render'),
      bind: function (event, handler) {
        eventRegistry[event] = handler;
      },
      trigger: function (command, param1, param2) {
        eventRegistry[command](param1, param2);
      }
    };
  };

  beforeEach(function () {
    model = jasmine.createSpyObj('model', ['ensureDigits', 'save', 'validate']);
    view = createViewStub();
    subject = new app.Controller(model, view);

    spyOn(window, 'serialize').and.callFake(function () {
      return formData;
    });
  });

  describe('input blur', function () {

    it('should validate the input field', function () {
      setupModel([noErrors]);

      view.trigger('inputBlur', inputElement, {});

      expect(model.validate).toHaveBeenCalledWith(jasmine.any(Object), jasmine.any(Object));
    });

  });

  describe('input SSN keypress', function () {

    it('should ensure the input is a digit', function () {
      var inputKey = 'a';
      var inputData = { inputName: inputKey };

      setupModel([noErrors]);      

      view.trigger('ssnKeypress', inputElement, inputKey);

      expect(model.ensureDigits).toHaveBeenCalledWith(inputData);
    });

  });

  describe('form submit', function () {

    it('should save the form', function () {
      setupModel([noErrors]);

      view.trigger('formSubmit', 'form element');

      expect(model.save).toHaveBeenCalledWith(jasmine.any(Object), jasmine.any(Function));
    });

  });

});