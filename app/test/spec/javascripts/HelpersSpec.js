describe('Helpers', function () {
  'use strict';

  describe('isEmptyString', function () {
    it('should return true when empty', function () {
      expect(window.isEmptyString('')).toEqual(true);
    });

    it('should return false when not empty', function () {
      expect(window.isEmptyString('fake')).toEqual(false);
    });    
  });

  describe('isEqual', function () {
    it('should return true when equal', function () {
      expect(window.isEqual(1, 1)).toEqual(true);
    });

    it('should return false when not equal', function () {
      expect(window.isEqual(1, 2)).toEqual(false);
    });    
  });

  describe('isValidSSN', function () {
    var i, l;

    var valid = [
      '121212121',
      '123948109'
    ];
    var invalid = [
      '123456789',
      '987654321',
      '111111111',
      '222222222',
      '333333333',
      '444444444',
      '555555555',
      '666666666',
      '777777777',
      '888888888',
      '999999999',
      '000000000'
    ];

    function test(input, output) {
      it('should return ' + output + ' when SSN is ' + input, function () {
        expect(window.isValidSSN(input)).toEqual(output);
      });
    }

    for (i = 0, l = valid.length; i < l; i++) {
      test(valid[i], true);
    }

    for (i = 0, l = invalid.length; i < l; i++) {
      test(invalid[i], false);
    }         
  });

  describe('isPOBox', function () {
    var i, l;

    var invalid = [
      'Box 123', 
      'Box-122', 
      'Box122',
      'box #123', 
      'box 122', 
      'box 123',      
      'number 123',
      'POB',
      'POB 123',
      'pobox123',
      'POBOX123'
    ];
    var valid = [
      'HC73 P.O. Box 217', 
      'P O Box125', 
      'P. O. Box', 
      'P.O 123', 
      'P.O. Box 123', 
      'P.O. Box', 
      'P.O.B 123',
      'P.O.B. 123', 
      'P.O.B.',     
      'P0 Box', 
      'PO 123', 
      'PO Box N', 
      'PO Box', 
      'PO-Box', 
      'Po Box', 
      'Post 123', 
      'Post Box 123', 
      'Post Office Box 123', 
      'Post Office Box', 
      'p box', 
      'p-o box', 
      'p-o-box', 
      'p.o box', 
      'p.o. box', 
      'p.o.-box', 
      'p.o.b. #123', 
      'p.o.b.', 
      'p/o box', 
      'po #123', 
      'po box 123', 
      'po box', 
      'po num123', 
      'po-box', 
      'pobox',
      'post office box'
    ];

    function test(input, output) {
      it('should return ' + output + ' when address is ' + input, function () {
        expect(window.isPOBox(input)).toEqual(output);
      });
    }

    for (i = 0, l = valid.length; i < l; i++) {
      test(valid[i], true);
    }

    for (i = 0, l = invalid.length; i < l; i++) {
      test(invalid[i], false);
    }   
  });

  describe('serialize', function () {

    var serializedData = {
      'first_name': 'dummyFirstName',
      'last_name': 'dummyLastName'
    };

    var form = (function () {
      var name, inputElement;
      var formElement = document.createElement('form');

      for (name in serializedData) {
        inputElement = document.createElement('input');
        inputElement.setAttribute('type','text');
        inputElement.setAttribute('name', name);
        inputElement.setAttribute('value', serializedData[name]);
        formElement.appendChild(inputElement);
      }      

      return formElement;
    })();

    it('should return if no form element is passed', function () {
      expect(window.serialize()).toBeUndefined();
    });

    it('should serialize a form element', function () {
      expect(window.serialize(form)).toEqual(serializedData);
    });
  });
});