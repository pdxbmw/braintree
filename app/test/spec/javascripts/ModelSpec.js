describe('Model', function () {
  'use strict';

  var subject, store;

  var formData = {
    'first_name': 'Bart',
    'last_name': 'Simpson',
    'address': '742 Evergreen Terrace Springfield, OR',
    'ssn': '945748084',
    'email': 'bart@simpson.com',
    'email_confirmation': 'homer@simpson.com'
  };

  beforeEach(function () {
    store = jasmine.createSpy('store');
    subject = new app.Model(store);
  });

  describe('validate', function () {

    it('should return isRequired error', function () {
      var inputData = { 'first_name': '' };
      var errObj = [{ name: 'first_name', errors: ['isRequired'] }];      
      
      expect(subject.validate(formData, inputData)).toEqual(errObj);
    });    

    it('should return invalidAddress error', function () {
      var inputData = { 'address': 'PO Box Cuz' };
      var errObj = [{ name: 'address', errors: ['invalidAddress'] }];      
      
      expect(subject.validate(formData, inputData)).toEqual(errObj);
    }); 

    it('should return invalidSSN error', function () {
      var inputData = { 'ssn': '123456789' };
      var errObj = [{ name: 'ssn', errors: ['invalidSSN'] }];      
      
      expect(subject.validate(formData, inputData)).toEqual(errObj);
    });  

    it('should return emailMismatch error', function () {
      var inputData = { 'email_confirmation': 'homer@simpson.com' };
      var errObj = [{ name: 'email_confirmation', errors: ['emailMismatch'] }];      
      
      expect(subject.validate(formData, inputData)).toEqual(errObj);
    });

    it('should return no errors when input field is not empty', function () {
      var inputData = { 'first_name': 'Homer' };
      var errObj = [{ name: 'first_name', errors: [] }];      
      
      expect(subject.validate(formData, inputData)).toEqual(errObj);
    });

    it('should return no errors for valid address', function () {
      var inputData = { 'address': '742 Evergreen Terrace Springfield, OR' };
      var errObj = [{ name: 'address', errors: [] }];      
      
      expect(subject.validate(formData, inputData)).toEqual(errObj);
    });    

    it('should return no errors for valid SSN', function () {
      var inputData = { 'ssn': '123451234' };
      var errObj = [{ name: 'ssn', errors: [] }];      
      
      expect(subject.validate(formData, inputData)).toEqual(errObj);
    });  

    it('should return no errors for valid email confirmation', function () {
      var inputData = { 'email': 'homer@simpson.com' };
      var errObj = [{ name: 'email', errors: [] }];      
      
      expect(subject.validate(formData, inputData)).toEqual(errObj);
    });              
  });

});