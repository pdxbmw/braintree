describe('View', function () {
  'use strict';

  var template, subject;

  var errorListItem = '<li>Error</li>';

  var errorObjs = [{
    name: 'first_name',
    errors: []
  },{
    name: 'last_name',
    errors: []
  }];

  var setupTemplate = function () {
    template.error.and.callFake(function () {
      return errorListItem;
    });
  };

  beforeEach(function () {
    template = jasmine.createSpyObj('template', ['error']);
    subject = new app.View(template);
  });

  describe('render', function () {

    describe('clearError', function () {
      var errorObj = errorObjs[0];

      it('should execute with error data', function () {
        spyOn(subject, '_clearError');
        
        subject.render('clearError', errorObj);
        
        expect(subject._clearError).toHaveBeenCalledWith(errorObj);
      });
      
      it('should remove input error class', function () {
        spyOn(window, 'qsn').and.callFake(function () { return { parentNode: 'fakeNode' }; });
        spyOn(window, 'qsc').and.callFake(function () { return { innerHTML: null }; });
        spyOn(window, 'removeClass');

        subject.render('clearError', errorObj);
        
        expect(window.removeClass).toHaveBeenCalled();
      });      
    });

    describe('showError', function () {
      var errorObj = errorObjs[0];

      it('should execute with error data', function () {
        spyOn(subject, '_showError');
        
        subject.render('showError', errorObj);
        
        expect(subject._showError).toHaveBeenCalledWith(errorObj);
      });
      
      it('should remove input error class', function () {
        spyOn(window, 'qsn').and.callFake(function () { return { parentNode: 'fakeNode' }; });
        spyOn(window, 'qsc').and.callFake(function () { return { innerHTML: null }; });
        spyOn(window, 'addClass');

        subject.render('showError', errorObj);
        
        expect(window.addClass).toHaveBeenCalled();
      });      
    });

  });

});